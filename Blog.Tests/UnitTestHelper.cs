﻿using AutoMapper;
using BLL;
using DAL;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Blog.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<BlogDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<BlogDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new BlogDbContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(BlogDbContext context)
        {
            //Articles
            Article art1 = new Article() { ArticleId = 1, Content = "Content article 1", Title = "Title article 1", Created = new DateTime(2001, 1, 11) };
            Article art2 = new Article() { ArticleId = 2, Content = "Content article 2", Title = "Title article 2", Created = new DateTime(2002, 2, 12) };

            context.Articles.Add(art1);
            context.Articles.Add(art2);


            //Tags
            Tag tag1 = new Tag() { TagId = 1, Name = "hunting" };
            Tag tag2 = new Tag() { TagId = 2, Name = "driving" };

            context.Tags.Add(tag1);
            context.Tags.Add(tag2);


            //Comments 
            Comment comment1 = new Comment() { CommentId = 1, ArticleId = 1, Content = "Comment 1", Created = new DateTime(2001, 1, 11) };

            context.Comments.Add(comment1);


            //add nav properties
            art1.Tags = new List<Tag>() { tag1 };
            art2.Tags = new List<Tag>() { tag2 };

            tag1.Articles = new List<Article>() { art1 };
            tag2.Articles = new List<Article>() { art2 };


            context.SaveChanges();
        }

        public static Mapper CreateMapper()
        {
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration); 
        }
    }
}
