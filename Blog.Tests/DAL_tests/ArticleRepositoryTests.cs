﻿using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Tests.DAL_tests
{
    class ArticleRepositoryTests
    {
        private DbContextOptions<BlogDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }

        [Test]
        public void ArticleRepository_GetAll_ReturnAllValues()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.AreEqual(2, result.Count);
            }
        }

        [Test]
        public void ArticleRepository_GetAll_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.IsNull(result[0].Tags);
                Assert.IsNull(result[0].Comments);
            }
        }

        [Test]
        public async Task ArticleRepository_GetByIdAsync_ReturnNull()
        {

            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                int id = 3;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNull(result);
            }
        }

        [Test]
        public async Task ArticleRepository_GetByIdAsync_ReturnEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Title, "Title article 1");
            }
        }

        [Test]
        public async Task ArticleRepository_GetByIdWithDetailsAsync_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdWithDetailsAsync(id);

                //assert
                Assert.AreEqual(result.Tags.Count, 1);
            }
        }

        [Test]
        public void ArticleRepository_GetAllWithDetails_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                var result =  repository.GetAllWithDetails().FirstOrDefault();

                //assert
                Assert.AreEqual(result.Tags.Count, 1);
            }
        }


        [Test]
        public async Task ArticleRepository_GetByIdAsync_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                ArticleRepository repository = new ArticleRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert
                Assert.IsNotNull(result);
                Assert.IsNull(result.Tags);
                Assert.IsNull(result.Comments);
            }
        }

        [Test]
        public async Task ArticleRepository_AddAsync_()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                ArticleRepository repository = new ArticleRepository(context);

                //act
                Article newArticle = new Article() { ArticleId = 3, Content = "Content article 3", Title = "Title article 3", Created = new DateTime(2003, 3, 13) };
                await repository.AddAsync(newArticle);
                context.SaveChanges();

                //assert
                Assert.That(context.Articles.Count(),Is.EqualTo(3)); 
            }
        }

        [Test]
        public async Task ArticleRepository_Update_UpdatesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                ArticleRepository repository = new ArticleRepository(context);

                //act
                Article updatedArticle = new Article() { ArticleId = 1, Content = "Updated", Title = "Title article 1", Created = new DateTime(2001, 1, 11) };
                repository.Update(updatedArticle);
                context.SaveChanges();

                Article result = await repository.GetByIdAsync(1);

                //assert
                Assert.AreEqual(result.Content, "Updated");                
            }
        }

        [Test]
        public void  ArticleRepository_DeleteById_DeletesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                ArticleRepository repository = new ArticleRepository(context);

                //act
                int id = 1; 
                repository.DeleteById(id);
                context.SaveChanges();

                //assert
                Assert.That(context.Articles.Count(), Is.EqualTo(1)); 
            }
        }

    }
}
