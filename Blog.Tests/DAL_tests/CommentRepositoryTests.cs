﻿using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace Blog.Tests.DAL_tests
{
    class CommentRepositoryTests
    {
        private DbContextOptions<BlogDbContext> _options; 

        [SetUp]
        public void SetUp()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions(); 
        }

        [Test]
        public void CommentRepository_GetAll_ReturnAllValues()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [Test]
        public void CommentRepository_GetAll_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.IsNull(result[0].Article);
            }
        }

        [Test]
        public async Task CommentRepository_GetByIdAsync_ReturnNull()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                int id = 3;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNull(result);
            }
        }

        [Test]
        public async Task CommentRepository_GetByIdAsync_ReturnEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Content, "Comment 1");
            }
        }

        [Test]
        public async Task CommentRepository_GetByIdWithDetailsAsync_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdWithDetailsAsync(id);

                //assert
                Assert.IsNotNull(result.Article);
            }
        }

        [Test]
        public void CommentRepository_GetAllWithDetails_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                var result = repository.GetAllWithDetails().FirstOrDefault();

                //assert
                Assert.IsNotNull(result.Article);
            }
        }


        [Test]
        public async Task CommentRepository_GetByIdAsync_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                CommentRepository repository = new CommentRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert
                Assert.IsNull(result.Article);
            }
        }

        [Test]
        public async Task CommentRepository_AddAsync_()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                CommentRepository repository = new CommentRepository(context);

                //act
                Comment newComment = new Comment() { CommentId = 2, Content="Comment 2",ArticleId = 1,  Created = new DateTime(2003, 3, 13) };
                await repository.AddAsync(newComment);
                context.SaveChanges();

                //assert
                Assert.That(context.Articles.Count(), Is.EqualTo(2));
            }
        }

        [Test]
        public async Task CommentRepository_Update_UpdatesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                CommentRepository repository = new CommentRepository(context);

                //act
                Comment updatedArticle = new Comment() { CommentId = 1, Content = "Updated comment",ArticleId = 1,  Created = new DateTime(2001, 1, 11) };
                repository.Update(updatedArticle);
                context.SaveChanges();

                Comment result = await repository.GetByIdAsync(1);

                //assert
                Assert.AreEqual(result.Content, "Updated comment");
            }
        }

        [Test]
        public void CommentRepository_DeleteById_DeletesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                CommentRepository repository = new CommentRepository(context);

                //act
                int id = 1;
                repository.DeleteById(id);
                context.SaveChanges();

                //assert
                Assert.That(context.Comments.Count(), Is.EqualTo(0));
            }
        }
    }
}
