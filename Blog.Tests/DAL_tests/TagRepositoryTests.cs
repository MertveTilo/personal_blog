﻿using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Tests.DAL_tests
{
    internal class TagRepositoryTests
    {
        private DbContextOptions<BlogDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }


        [Test]
        public void TagRepository_GetAll_ReturnAllValues()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.AreEqual(2, result.Count);
            }
        }

        [Test]
        public void TagRepository_GetAll_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                var result = repository.GetAll().ToList();

                //assert
                Assert.IsNull(result[0].Articles);
            }
        }

        [Test]
        public async Task TagRepository_GetByIdAsync_ReturnNull()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                int id = 3;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNull(result);
            }
        }

        [Test]
        public async Task TagRepository_GetByIdAsync_ReturnEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Name, "hunting");
            }
        }

        [Test]
        public async Task TagRepository_GetByIdAsync_DoesntLoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdAsync(id);

                //assert
                Assert.IsNotNull(result);
            }
        }

        [Test]
        public async Task TagRepository_GetByIdWithDetailsAsync_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                int id = 1;
                var result = await repository.GetByIdWithDetailsAsync(id);

                //assert
                Assert.AreEqual(result.Articles.Count, 1);
            }
        }

        [Test]
        public void TagRepository_GetAllWithDetails_LoadNavProperties()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                TagRepository repository = new TagRepository(context);

                //act
                var result = repository.GetAllWithDetails().FirstOrDefault();

                //assert
                Assert.AreEqual(result.Articles.Count, 1);
            }
        }

        [Test]
        public async Task TagRepository_AddAsync_()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                TagRepository repository = new TagRepository(context);

                //act
                Tag newArticle = new Tag() { TagId = 3, Name = "fishing" }; 
                await repository.AddAsync(newArticle);
                context.SaveChanges();

                //assert
                Assert.That(context.Tags.Count(), Is.EqualTo(3));
            }
        }

        [Test]
        public async Task TagRepository_Update_UpdatesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                TagRepository repository = new TagRepository(context);

                //act
                Tag updatedArticle = new Tag() { TagId = 1, Name="updated" };
                repository.Update(updatedArticle);
                context.SaveChanges();

                Tag result = await repository.GetByIdAsync(1);

                //assert
                Assert.AreEqual(result.Name, "updated");
            }
        }

        [Test]
        public void TagRepository_DeleteById_DeletesEntity()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arange 
                TagRepository repository = new TagRepository(context);

                //act
                int id = 1;
                repository.DeleteById(id);
                context.SaveChanges();

                //assert
                Assert.That(context.Tags.Count(), Is.EqualTo(1));
            }
        }
    }
}
