﻿using BLL.Exceptions;
using BLL.Models;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Blog.Tests.BLL_tests
{
    public class AticleServiceTests
    {
        private DbContextOptions<BlogDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }

        [Test]
        public async Task ArticleService_GetByFilterAsync_ReturnsCorrectModels()
        {
            using (BlogDbContext context = new BlogDbContext(_options))
            {
                //arrange
                FilterArticleModel filter = new FilterArticleModel() { Tag = "hunting", Created = new DateTime(2001, 1, 11) };

                UnitOfWork unitOfWork = new UnitOfWork(context); 
                ArticleService service = new ArticleService(unitOfWork, UnitTestHelper.CreateMapper());

                //act
                var result = await service.GetByFilterAsync(filter);

                //assert
                foreach (var article in result)
                    Assert.IsTrue(article.Tags.Count() != 0);

                Assert.AreEqual(result.ToList().Count(), 1); 
            }
        }

    }
}
