﻿using BLL.Exceptions;
using BLL.Models;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Tests.BLL_tests
{
    class TagServiceTests
    {

        private IEnumerable<TagModel> GetTestTagsModels()
        {
            yield return new TagModel() { TagId = 1, Name = "fishing" };
            yield return new TagModel() { TagId = 2, Name = "driving" }; 
        }

        private IEnumerable<Tag> GetTestTagsEntities()
        {
            yield return new Tag() { TagId = 1, Name = "fishing" };
            yield return new Tag() { TagId = 2, Name = "driving" };
        }



        [Test]
        public async Task TagService_AddAsync_AddsModel()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(unit => unit.TagRepository.AddAsync(It.IsAny<Tag>()));

            TagService service = new TagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapper());
            TagModel tag = new TagModel() { TagId = 1, Name = "fishing" };


            //action 
            await service.AddAsync(tag);


            //asserts
            mockUnitOfWork.Verify(x => x.TagRepository.AddAsync(It.Is<Tag>(t => t.TagId == tag.TagId && t.Name ==tag.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public  void TagService_AddAsync_ThrowsErrorWhenNameIsEmpty()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(unit => unit.TagRepository.AddAsync(It.IsAny<Tag>()));

            TagService service = new TagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapper());
            TagModel tag = new TagModel() { TagId = 1, Name = "" };


            //asserts
            Assert.ThrowsAsync<BLLModelException>(() => service.AddAsync(tag)); 
        }

        [Test]
        public void TagService_GetAll_ReturnsModels()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(unit => unit.TagRepository.GetAll())
                           .Returns(GetTestTagsEntities().AsQueryable());

            TagService service = new TagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapper());

            var expected = GetTestTagsModels().ToList();


            //action
            var actual = service.GetAll().ToList();

            //assert
            for (int i = 0; i < expected.Count; ++i)
            {
                Assert.AreEqual(actual[0].TagId, expected[0].TagId); 
                Assert.AreEqual(actual[0].Name, expected[0].Name); 
            }
        }
    }
}
