﻿using BLL.Exceptions;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation
{
    public static class ModelValidator
    {
        //TODO  check if id is  <= 0 and throw an error if it is

        public static void ArticleValidation(ArticleModel model)
        {
            if (string.IsNullOrEmpty(model.Title?.Trim()))
                throw new BLLModelException("Article`s title is empty");

            if (string.IsNullOrEmpty(model.Content?.Trim()))
                throw new BLLModelException("Article`s content is emtpy"); 
        }

        public static void TagValidation(TagModel model)
        {
            if (string.IsNullOrEmpty(model.Name?.Trim()))
                throw new BLLModelException("Tag`s name is empty"); 
        }

        public static void CommentValidation(CommentModel model)
        {   
            if (string.IsNullOrEmpty(model.Content?.Trim()))
                throw new BLLModelException("Comment`s content is empty"); 

            if(model.ArticleId  <= 0)
                throw new BLLModelException("Comment refers to article with wrong id");

        }

    }
}
