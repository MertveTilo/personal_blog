﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CommentService : ICommentService
    {
        private IUnitOfWork _unitOfWork;
        private Mapper _mapper; 

        public CommentService(IUnitOfWork unitOfWork, Mapper mapper )
        {
            if (unitOfWork == null)
                throw new ArgumentNullException($"{nameof(unitOfWork)} can`t be null");

            if (mapper == null)
                throw new ArgumentNullException($"{nameof(mapper)} can`t be null");

            _unitOfWork = unitOfWork;
            _mapper = mapper; 
        }
        
        public async Task AddAsync(CommentModel model)
        {
            if (model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.CommentValidation(model);
            }
            catch (BLLModelException)
            {
                throw;
            }

            Comment newComment = _mapper.Map<Comment>(model);
            await _unitOfWork.CommentRepository.AddAsync(newComment);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            //TODO add hanling of exception when model with id doesn`t exist
            _unitOfWork.CommentRepository.DeleteById(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<CommentModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<CommentModel>>(await _unitOfWork.CommentRepository.GetAll().ToListAsync()); 
        }

        public async Task<CommentModel> GetByIdAsync(int id)
        {
            Comment commentFromDb = await _unitOfWork.CommentRepository.GetByIdAsync(id);
            return _mapper.Map<CommentModel>(commentFromDb); 
        }

        public async Task<IEnumerable<CommentModel>> GetCommentsByArticleIdAsync(int articleId, int amount)
        {
            //TODO test what happens when amount is negative
            var comments = await _unitOfWork.CommentRepository.GetAll()
                                                              .Where(comment => comment.ArticleId == articleId)
                                                              .Take(amount)
                                                              .ToListAsync();

            return _mapper.Map<IEnumerable<CommentModel>>(comments); 
        }

        public async Task UpdateAsync(CommentModel model)
        {
            if (model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.CommentValidation(model);
            }
            catch (BLLModelException)
            {
                throw;
            }

            Comment comment = _mapper.Map<Comment>(model);
            _unitOfWork.CommentRepository.Update(comment);

            await _unitOfWork.SaveAsync();
        }
    }
}
