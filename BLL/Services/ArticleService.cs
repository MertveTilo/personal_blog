﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ArticleService : IArticleService
    {
        private IUnitOfWork _unitOfWork;
        private Mapper _mapper;

        /// <exception cref="ArgumentNullException"> is thrown when one of the passed arguments is null</exception>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public ArticleService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException($"{nameof(unitOfWork)} can`t be null");

            if (mapper == null)
                throw new ArgumentNullException($"{nameof(mapper)} can`t be null");

            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async  Task AddAsync(ArticleModel model)
        {
            if (model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.ArticleValidation(model);
            }
            catch (BLLModelException)
            {
                throw;
            }

            Article newArticle = _mapper.Map<Article>(model);

            await _unitOfWork.ArticleRepository.AddAsync(newArticle);

            await _unitOfWork.SaveAsync();

        }

        public async Task AddTagToArticleAsync(int articleId, int tagId)
        {
            Article article = await _unitOfWork.ArticleRepository.GetByIdWithDetailsAsync(articleId);
            //TODO add checking if article already contains this tag
            Tag tag = await _unitOfWork.TagRepository.GetByIdAsync(tagId);

            if (article == null)
                throw new NoEntityWithId($"Article with id {articleId} doesn`t exist");
            else if (tag == null)
                throw new NoEntityWithId($"Tag with id {tagId} doesn`t exist"); 

            article.Tags.Add(tag);

            await _unitOfWork.SaveAsync(); 
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            //TODO add exception handling if entity with id doesn`t exist
            _unitOfWork.ArticleRepository.DeleteById(modelId);

            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<ArticleModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<ArticleModel>>(await _unitOfWork.ArticleRepository.GetAllWithDetails().ToListAsync());
        }

        public async  Task<IEnumerable<ArticleModel>> GetByFilterAsync(FilterArticleModel filter)
        {
            if (filter == null)
                throw new ArgumentNullException($"{nameof(filter)} can`t be null");

            var builtQuery = _unitOfWork.ArticleRepository.GetAllWithDetails();

            //add filter by date
            if (filter.Created != default(DateTime))
                builtQuery = builtQuery.Where(article => article.Created == filter.Created);
            //add filter by tag name if 
            if (!string.IsNullOrEmpty(filter.Tag))
                builtQuery = builtQuery.Where(article => article.Tags.Where(tag=>tag.Name == filter.Tag).Count() !=  0);

            return _mapper.Map<IEnumerable<ArticleModel>>(await builtQuery.ToListAsync()); 
        }

        public async Task<ArticleModel> GetByIdAsync(int id)
        {
            Article articleFromDb = await _unitOfWork.ArticleRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<ArticleModel>(articleFromDb);
        }

        public async  Task RemoveTagFromArticleAsync(int articleId, int tagId)
        {
            Article article = await _unitOfWork.ArticleRepository.GetByIdWithDetailsAsync(articleId);

            if (article == null)
                throw new NoEntityWithId($"Article with id {articleId} doesn`t exist");

            Tag tagToRemove = article.Tags.First(tag => tag.TagId == tagId);

            if (tagToRemove != null)
                article.Tags.Remove(tagToRemove);

            await _unitOfWork.SaveAsync(); 
        }

        public async Task UpdateAsync(ArticleModel model)
        {
            if (model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.ArticleValidation(model); 
            }
            catch(BLLModelException)
            {
                throw; 
            }

            Article article = _mapper.Map<Article>(model);
            _unitOfWork.ArticleRepository.Update(article);

            await _unitOfWork.SaveAsync();
        }
    }
}
