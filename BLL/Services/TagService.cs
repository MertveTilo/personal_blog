﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TagService : ITagService
    {
        private IUnitOfWork _unitOfWork;
        private Mapper _mapper; 

        public TagService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException($"{nameof(unitOfWork)} can`t be null"); 

            if(mapper == null)
                throw new ArgumentNullException($"{nameof(mapper)} can`t be null");

            _mapper = mapper;
            _unitOfWork = unitOfWork; 
        }

        public async Task AddAsync(TagModel model)
        {
            if(model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.TagValidation(model); 
            }
            catch(BLLModelException)
            {
                throw; 
            }

            Tag newTag = _mapper.Map<Tag>(model);

            await _unitOfWork.TagRepository.AddAsync(newTag);

            await _unitOfWork.SaveAsync(); 
        }

        public async Task<IEnumerable<TagModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<TagModel>>(await _unitOfWork.TagRepository.GetAll().ToListAsync()); 
        }

        public async Task<TagModel> GetByIdAsync(int id)
        {
            Tag tagFromDb = await _unitOfWork.TagRepository.GetByIdAsync(id);

            return _mapper.Map<TagModel>(tagFromDb); 
        }

        public async Task UpdateAsync(TagModel model)
        {
            if (model == null)
                throw new ArgumentNullException($"{nameof(model)} can`t be null");

            try
            {
                ModelValidator.TagValidation(model);
            }
            catch (BLLModelException)
            {
                throw;
            }

            Tag updatedTag = _mapper.Map<Tag>(model);
            _unitOfWork.TagRepository.Update(updatedTag);

            await _unitOfWork.SaveAsync(); 
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            _unitOfWork.TagRepository.DeleteById(modelId);
            await _unitOfWork.SaveAsync(); 
        }
    }
}
