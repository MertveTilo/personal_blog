﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using BLL.Models;
using DAL.Entities;

namespace BLL
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            //Article
            CreateMap<Article, ArticleModel>().ForMember(dto => dto.Tags, 
                                                         opt=>opt.MapFrom(entity=>entity.Tags.Select(tag=>tag.Name)));
            CreateMap<ArticleModel, Article>();

            //Tag
            CreateMap<Tag, TagModel>();
            CreateMap<TagModel, Tag>();

            //Comment
            CreateMap<Comment, CommentModel>();
            CreateMap<CommentModel, Comment>(); 
        }
    }
}
