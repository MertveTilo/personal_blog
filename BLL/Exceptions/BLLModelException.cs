﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Exceptions
{
    public class BLLModelException:Exception
    {
        public BLLModelException() :base()
        {

        }

        public BLLModelException(string message): base(message)
        {

        }

        public BLLModelException(string message, Exception exception): base(message, exception)
        {

        }
    }
}
