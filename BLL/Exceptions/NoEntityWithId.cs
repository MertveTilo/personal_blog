﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Exceptions
{
    public class NoEntityWithId: Exception
    {
        public NoEntityWithId() : base()
        {

        }

        public NoEntityWithId(string message) : base(message)
        {

        }

        public NoEntityWithId(string message, Exception exception) : base(message, exception)
        {

        }
    }
}
