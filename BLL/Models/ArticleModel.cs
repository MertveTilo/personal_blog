﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ArticleModel
    {
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }
        public ICollection<string> Tags { get; set; }
    }
}
