﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class CommentModel
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }

        public int ArticleId { get; set; }
    }
}
