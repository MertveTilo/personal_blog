﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class FilterArticleModel
    {
        public string Tag { get; set; }
        public DateTime Created { get; set; }
    }
}
