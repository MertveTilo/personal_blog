﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IArticleService:ICrud<ArticleModel>
    {

        /// <exception cref="ArgumentNullException">is thrown when the passed argument is equal to null</exception>
        /// <param name="filter"></param>
        public Task<IEnumerable<ArticleModel>> GetByFilterAsync(FilterArticleModel filter);

        /// <exception cref="NoEntityWithId"> is thrown when entity with id doesn`t exist</exception>
        /// <param name="articleId"></param>
        /// <param name="tagId"></param>
        public Task AddTagToArticleAsync(int articleId, int tagId);

        /// <exception cref = "NoEntityWithId" > is thrown when entity with id doesn`t exist</exception>
        public Task RemoveTagFromArticleAsync(int articleId, int tagId); 
    }
}
