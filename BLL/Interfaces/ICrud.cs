﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICrud<TModel> where TModel : class
    {
        Task<IEnumerable<TModel>> GetAllAsync();

        Task<TModel> GetByIdAsync(int id);

        /// <exception cref="ArgumentNullException"> is thrown when passed argument is equal to null</exception>
        /// <exception cref="BLLModelException"> is thrown when some of model`s data is invalid</exception>
        /// <param name="model"></param>
        Task AddAsync(TModel model);

        /// <exception cref="ArgumentNullException"> is thrown when passed argument is equal to null</exception>
        /// <exception cref="BLLModelException"> is thrown when some of model`s data is invalid</exception>
        /// <param name="model"></param>
        Task UpdateAsync(TModel model);

        Task DeleteByIdAsync(int modelId);
    }
}
