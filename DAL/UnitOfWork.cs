﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork:IUnitOfWork, IDisposable
    {
        private BlogDbContext _db;

        private bool _disposed; 

        private ArticleRepository _articleRepository; 
        private TagRepository _tagRepository; 
        private CommentRepository _commentRepository; 

        public  UnitOfWork(BlogDbContext db)
        {
            _db = db;
            _disposed = false; 
        }

    //Implement IUnitOfWork
        public IRepository<Article> ArticleRepository 
        {
            get 
            {
                if (_articleRepository == null)
                    _articleRepository = new ArticleRepository(_db);

                return _articleRepository; 

            }
        }
        public IRepository<Tag> TagRepository
        {
            get
            {
                if (_tagRepository == null)
                    _tagRepository = new TagRepository(_db);

                return _tagRepository; 
            }
        }
        public IRepository<Comment> CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new CommentRepository(_db);

                return _commentRepository; 

            }
        }

        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync(); 
        }

    //Impelement IDisposable
        public void Dispose()
        {
            Dispose(true); 
        }

        protected virtual void Dispose(bool disposing)
        {
            if(_disposed)
                return;

            if(disposing)
                _db.Dispose();

            _disposed = true;
        }

    }
}
