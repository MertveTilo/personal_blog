﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }

        //Navigation properties
        public int ArticleId { get; set; }
        public Article Article { get; set; }
    }
}
