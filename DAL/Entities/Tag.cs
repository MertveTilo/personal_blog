﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Tag
    {
        public int TagId { get; set; }
        public string Name { get; set; }

        //Navigation properties
        public ICollection<Article> Articles {get;set;} 
    }
}
