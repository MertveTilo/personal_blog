﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Article> ArticleRepository { get; } 
        IRepository<Tag> TagRepository { get; }
        IRepository<Comment> CommentRepository { get; }

        Task<int> SaveAsync(); 
    }
}
