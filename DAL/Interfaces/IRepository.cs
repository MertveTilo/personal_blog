﻿using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        Task<T> GetByIdAsync(int id);
        IQueryable<T> GetAllWithDetails();
        Task<T> GetByIdWithDetailsAsync(int id);
        Task AddAsync(T entity);
        void Update(T entity);
        void DeleteById(int id);


    }
}
