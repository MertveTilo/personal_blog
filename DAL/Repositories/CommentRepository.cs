﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CommentRepository: IRepository<Comment>
    {
        private BlogDbContext _db;

        public CommentRepository(BlogDbContext db)
        {
            if (db == null)
                throw new ArgumentNullException($"{nameof(db)} was null");

            _db = db;
        }

    //IRepository implementation
    
        //Get
        public IQueryable<Comment> GetAll()
        {
            return _db.Comments.Select(comment => comment); 
        }

        public IQueryable<Comment> GetAllWithDetails()
        {
            return _db.Comments.Include(comment => comment.Article); 
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _db.Comments.FindAsync(id); 
        }

        public async Task<Comment> GetByIdWithDetailsAsync(int id)
        {
            return await _db.Comments.Where(comment => comment.CommentId == id)
                                     .Include(comment => comment.Article)
                                     .FirstOrDefaultAsync(); 
        }

        //Add
        public async Task AddAsync(Comment entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            await _db.Comments.AddAsync(entity); 
        }

        //Update
        public void Update(Comment entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            _db.Comments.Update(entity); 
        }

        //Delete
        public void DeleteById(int id)
        {
            Comment entity = new Comment() { CommentId = id };
            _db.Comments.Remove(entity); 
        }
    }
}
