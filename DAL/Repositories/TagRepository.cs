﻿
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TagRepository : IRepository<Tag>
    {
        private BlogDbContext _db; 

        public TagRepository(BlogDbContext db)
        {
            if (db == null)
                throw new ArgumentNullException($"{nameof(db)} was null");

            _db = db; 
        }

    //IRepository implementation

        //Get
        public IQueryable<Tag> GetAll()
        {
            return _db.Tags.Select(tag => tag);
        }

        public IQueryable<Tag> GetAllWithDetails()
        {
            return _db.Tags.Include(tag => tag.Articles);
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _db.Tags.FindAsync(id);
        }

        public async Task<Tag> GetByIdWithDetailsAsync(int id)
        {
            return await _db.Tags
                .Where(tag => tag.TagId == id)
                .Include(tag => tag.Articles)
                .FirstOrDefaultAsync();
        }

        
        //Add
        public async Task AddAsync(Tag entity)
        {
            if (entity== null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            await _db.Tags.AddAsync(entity);
        }

        //Update
        public void Update(Tag entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            _db.Tags.Update(entity);
        }

        //Delete
        public void DeleteById(int id)
        {
            Tag tagToDelete = new Tag { TagId = id };
            _db.Tags.Remove(tagToDelete);
        }
    }
}
