﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private BlogDbContext _db; 

        public ArticleRepository(BlogDbContext db)
        {
            if (db == null)
                throw new ArgumentNullException($"{nameof(db)} was null"); 

            _db = db; 
        }

    //IRepository implementation

        //Get
        public IQueryable<Article> GetAll()
        {
            return _db.Articles.Select(art => art); 
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            Article entity = await _db.Articles.FindAsync(id);
            
            return entity; 
        }

        public IQueryable<Article> GetAllWithDetails()
        {
            return _db.Articles
                .Include(art => art.Comments)
                .Include(art => art.Tags);
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            Article entity = await _db.Articles
                .Where(art => art.ArticleId == id)
                .Include(art=>art.Comments)
                .Include(art=> art.Tags)
                .FirstOrDefaultAsync(); 

            return entity; 
        }

        //Add
        public async Task AddAsync(Article entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            await _db.Articles.AddAsync(entity); 
        }

        //Update
        public void Update(Article entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"{nameof(entity)} was null");

            _db.Articles.Update(entity); 
        }

        //Delete
        public  void DeleteById(int id)
        {
            Article entity = new Article() { ArticleId = id };

            _db.Articles.Remove(entity);
        }
    }
}
