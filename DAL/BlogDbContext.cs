﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public BlogDbContext(DbContextOptions<BlogDbContext> options):base(options)
        {
            
        }

    }
}
