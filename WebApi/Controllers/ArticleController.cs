﻿using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        //GET: /api/article/filter?tag=tag_name&created=2021.20.01
        [HttpGet("filter")]
        public async Task<ActionResult<IEnumerable<ArticleModel>>> GetArticlesByFilter([FromQuery] FilterArticleModel filter)
        {
            IEnumerable<ArticleModel> filteredArticles = await _articleService.GetByFilterAsync(filter);

            return Ok(filteredArticles);
        }

        //GET:/api/article/all
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ArticleModel>>>GetAll()
        {
            return Ok(await _articleService.GetAllAsync()); 
        }

        //GET:/api/article/{id}
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ArticleModel>> GetById(int id)
        {
            ArticleModel article = await _articleService.GetByIdAsync(id);
            if (article == null)
                return BadRequest();
            else
                return Ok(article); 
        }


        //POST: /api/article/
        [HttpPost]
        public async Task<ActionResult> AddNew([FromForm]ArticleModel model)
        {
            try
            {
                await _articleService.AddAsync(model);
            }
            catch (BLLModelException ex)
            {
                return BadRequest(ex.Message);
            }

            return StatusCode(204);
        }

        //PUT /api/article/add/tag?articleId=1&tagId=1
        [HttpPut("add/tag")]
        public async Task<ActionResult> AddTagToArticle(int articleId, int tagId)
        {
            try
            {
                await _articleService.AddTagToArticleAsync(articleId, tagId);
            } 
            catch(NoEntityWithId ex)
            {
                return BadRequest(ex.Message); 
            }

            return StatusCode(204); 
        }

        //PUT /api/article/remove/tag?articleId=1&tagId=1
        [HttpPut("remove/tag")]
        public async Task<ActionResult> RemoveTagFromArticle(int articleId, int tagId)
        {
            try
            {
                await _articleService.RemoveTagFromArticleAsync(articleId, tagId);
            } 
            catch(NoEntityWithId ex)
            {
                return BadRequest(ex.Message); 
            }

            return StatusCode(204); 

        }

        //DELETE /api/article/delete?articleId=1
        [HttpDelete("delete")]
        public async Task<ActionResult> DeleteArticle(int articleId)
        {
            await _articleService.DeleteByIdAsync(articleId);

            return StatusCode(204);
        }


        
    }
}
