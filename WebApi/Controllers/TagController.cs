﻿using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {

        private ITagService _tagService; 

        public TagController(ITagService tagService)
        {
            _tagService = tagService; 
        }

        //POST: api/tag/
        [HttpPost]
        public async Task<ActionResult> AddNew([FromForm]TagModel model)
        {
            try
            {
                await _tagService.AddAsync(model); 
            }
            catch(BLLModelException ex)
            {
                return BadRequest(ex.Message); 
            }

            return StatusCode(204); 
        }

    }
}
